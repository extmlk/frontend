## Synopsis
Validating Swedish SSN's is not an entirely straight-forward procedure. Here's a JS/jQuery solution that will help.

### Prerequisites

This script is dependent on the npm package [is-personnummer](https://github.com/personnummer/js) to validate if the SSN is actually a valid Swedish personnummer or not. It can be installed via either npm or bower, depending on what your project is using.

1. `npm i -D is-personnummer`
2. `bower install is-personnummer`

### Usage

1. Pass in your input field in the `var $SSN` on line 51
2. Do what you want with the result on line 104

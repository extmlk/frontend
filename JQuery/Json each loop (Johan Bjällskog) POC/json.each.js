$(function() {
    callJsonEach(); 
});
    
function callJsonEach() {
    var json = $('.js-json-each');
    var loader = json.siblings(".loader");

    if (!json.length) {
        return;
    }

    var stringResults = ""; 
    var url = json.data('url');

    $.ajax({
        type : 'GET',
        dataType : 'json',
        context: this,
        url: url,

        beforeSend:function() {             
            loader.addClass("loader__show");
        },
        success:function(data) {
            loader.removeClass("loader__show");

            if(JSON.stringify(data) != '{}') {
                
                $.each(data, function(i, results){                   

                        stringResults += "<li class='category-list-item'><article class='category-list-item__product-info'><figure><img src='img/istone1.jpg' alt='text here' /></figure>";
                        stringResults += "<div class='category-list-item__text-container js-open-lightbox js-get-content'><h2 class='name'>"+ results.title +"</h2>";
                        stringResults += "<p class='category-list-item__article-nr'>Art. nr 01-01</p></div><p class='desc'>"+ results.body +"</p></article>";
                        stringResults += "<div class='category-list-item__stock-level'><address>Strängnäs<span class='address__span--in-stock'>10 in stock</span></address></div>";
                        stringResults += "<fieldset class='category-list-item__buy-area'><p class='category-list-item__price'>130 :-</p><button class='button js-json-post' type='submit' data-url='https://jsonplaceholder.typicode.com/posts'>Add</button><div class='loader js-popover-loader'><span class='loader__spinner'></span></div></fieldset></li>";
                });

                json.append(stringResults);

                //call js-functions that needs to be hooked again
                callJsonPost(); //hooking the buttons

            } else {
                json.append("<p>No results</p>");
            }

        }, 
        error: function(jqXHR, textStatus, errorThrown) { 
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown); 
        } 
    });
}
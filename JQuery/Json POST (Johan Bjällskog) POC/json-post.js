$(function() {
    callJsonPost(); 
});
    
function callJsonPost() {
    var jsonPost = $('.js-json-post');
    
    if (!jsonPost.length) {
        return;
    }

    jsonPost.on('click', function (event) {

        var url = jsonPost.data('url');
        var loader = $(this).siblings(".loader");
        var jsonData = {
            "userId": 1,
            "id": 1,
            "title": "new header",
            "body": "new text"
        };

        $.ajax({
            type : 'POST',
            dataType : 'json',
            data : jsonData, //add JSON.stringify() if needed.
            url: url,

            beforeSend:function() {             
                loader.addClass("loader__show");
            },
            success:function(data) {
                loader.removeClass("loader__show");

                console.log("postat");
                console.log(data);
                

                //call js-functions that needs to be hooked again

            }, 
            error: function(jqXHR, textStatus, errorThrown) { 
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown); 
            } 
        });
    });
}
$(function () {
    $(document).on('click', '.js-accordion__trigger', function () {
        $(this).siblings('.js-accordion__content').toggle('blind', 'ease-in', 200);
        $(this).find('span').toggleClass('faq-caret');
        $(this).find('span').toggleClass('faq-caret--open');
    });
});

$(function () {
     //box follow scroll for ipad and desktop

    var browserWidth = "";
    var sticky = $('.js-sticky');
    var header = $('.header').height();
    var scrolling = false;
    var top = 0;

    if (!sticky.length) {
        return false;
    }

    stickyBox();

    $(window).resize(function() {       
        stickyBox();
    });

    $(window).scroll(function(event) {
        if (scrolling === false) {
            return false;
        } 
        // what the y position of the scroll is
        var y = $(this).scrollTop() + header;

        // whether that's below the header
        if (y >= top) {          
            sticky.attr("style", "position:fixed; top:"+header+"px; width:"+sticky.width()+"px");
        } else {
            sticky.attr("style", "");
        }     
    });  

    function stickyBox() {
        browserWidth = $(window).width();

        if (browserWidth > 1025) {
            top = sticky.parent().offset().top - parseFloat(sticky.css('marginTop').replace(/auto/, 0));     
            scrolling = true;
        } else {
            scrolling = false;
            sticky.attr("style", "");
        }
    }
});

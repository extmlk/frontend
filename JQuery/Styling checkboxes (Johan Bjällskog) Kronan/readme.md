1. put js script in body
2. put css file in head
3. add picture in project
4. activate in js code as bellow

$(document).ready(function () {
	//for checkbox and radio buttons
	$('input').iCheck({
	    	checkboxClass: 'icheckbox_minimal-green',
    		radioClass: 'iradio_minimal-green',
    		increaseArea: '20%' // optional
  	});
  	
	$('input').on('ifChanged', function(event){
    		$(this).trigger('change');
    	});
});
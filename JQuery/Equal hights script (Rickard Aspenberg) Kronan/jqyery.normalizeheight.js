/*
 * jQuery Normalize height v0.9.6
 *
 * Copyright 2012, Rickard Aspenberg
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * February 2012
 *
 * General function to set the same height per row on floating elements.
 * You have the option of specifying child-elements within the wrapping element,
 * so that these also gets same height per row (for nice alignment within the boxes).
 *
 * Inparams:
 * elsStr (optional) = Eventual child-elements within the wrapping element where we wish same height/row: (".big,.ingress,h1")
 * skipWinResizeEvent (optional) = Will not re-calculate on browser window resize
 *
 * Examples:
 * $("#main li").normalizeHeight();
 * $("#main li").normalizeHeight(".big,.ingress,h1");
 *
 * Skip resize:
 * $("#main li").normalizeHeight(".big,.ingress,h1", true);
 */


;(function ($) {
    // Get the position of an element
    $.getPosition = function (el, pType) {
        var p = $(el);
        var position = p.offset();
        if (pType == 'top'){
            return position.top;
        }
        if (pType == 'bottom'){
            return position.bottom;
        }
        if (pType == 'left') {
            return position.left;
        }
        if (pType == 'right'){
            return position.right;
        }
    };

    var elementsList = new Array();

    // Used in function normalizeHeight()
    // Create object to be used in function
    $.createNormalizeHeightObj = function (_el) {
        // Create new object to store height and elements
        var $el = $(_el);
        var obj = new Object;
        obj.el = $el;
        obj.height = $el.height();
        return obj;
    };

    // Set all wrappers to the same height
    $.setAllSameHeight = function (_el) {
        var _h = 0;
        var $el = $(_el);
        $el.each(function() {
            _h = Math.max(_h, $(this).outerHeight());
        });
        $el.height(_h);
    };

    // Used in function normalizeHeight() found here above
    // General function to set all elements to the same height
    // els      = Array with strings pointing to an element
    // objList  = List of objects
    // No height will be set on elements containing the class: ignoreEqHeight
    $.normalizeHeight_checkAndSetBatchHeight = function (els, objList) {

        // Iterate through all elements to check height on
        if (els.length > 0) {
            $(els).each(function() {
                var tmpEl = String(this);
                var _h = 0;
                // Iterate through object list to get height
                $(objList).each(function() {
                    _h = Math.max(_h, $(tmpEl, this.el).outerHeight());
                });

                // Set height
                $(objList).each(function() {
                    $(tmpEl, this.el).not('.ignoreEqHeight').outerHeight(_h);
                });
            });
        }

        var wrapperHeight = 0;
        // Get height per row
        $(objList).each(function() {
            wrapperHeight = Math.max(wrapperHeight, this.el.outerHeight());
        });

        // Set height per row
        $(objList).each(function() {
            this.el.height(wrapperHeight);
        });
    };

    $.delay = (function() {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    // Main function
    $.normalizeHeight = function ($wrapper, elsStr, skipWinResizeEvent) {

        $wrapper.css('height', 'auto');

        if (!elsStr){
            elsStr = '';
        }
        var obj = new Object();
        obj.elWrapper   = $wrapper;
        obj.elsStr      = elsStr;

        // Resize after browser window has resized
        if (!skipWinResizeEvent) {
            $(window).resize(function() {
                $.delay(function() {
                    // Reset height on all elements
                    var _els = els.length > 0 ? elsStr : '';

                    $.each(elementsList, function(index, obj) {
                        obj.elWrapper.css('height', 'auto');
                        $(obj.elsStr, obj.elWrapper).css('height', 'auto');
                        // Reset height
                        $.normalizeHeight(obj.elWrapper, obj.elsStr, true);
                    });

                }, 400);
            });
        }

        // Create Array out of elements
        var els = elsStr.split(',');

        var oldPositionTop = 0;
        var objList = new Array();

        $wrapper.each(function(indx) {

            var _top = $.getPosition(this, 'top');

            // New row
            //if (_top != oldPositionTop && objList.length != 0) {
            if (_top != oldPositionTop) {
                $.normalizeHeight_checkAndSetBatchHeight(els, objList);

                // Clear Array
                objList = [];
                _top = $.getPosition(this, 'top');
            }
            // END - New row

            // Add object to Array
            objList[objList.length] = $.createNormalizeHeightObj(this);

            if (indx == ($wrapper.length - 1)){
                $.normalizeHeight_checkAndSetBatchHeight(els, objList);
            }

            oldPositionTop = _top;
        });
        return $wrapper;
    };

    $.fn.normalizeHeight = function (elsStr, skipWinResizeEvent) {
        var wrapper = this;

        if (!elsStr){
            elsStr = '';
        }
        var obj = new Object();
        obj.elWrapper   = wrapper;
        obj.elsStr      = elsStr;

        elementsList[elementsList.length] = obj;

        return $.normalizeHeight(wrapper, elsStr, skipWinResizeEvent);
    };


})(jQuery);

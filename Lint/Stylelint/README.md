# Stylelint

### Helpful links
* [Stylelint: available rules](https://stylelint.io/user-guide/rules/)
* [Create your own rules file for Stylelint](https://stylelint.io/demo/)
* [Stylelint plugin for editors](https://stylelint.io/user-guide/complementary-tools/) (*highly recommended*)

***

### Example files
**`.stylelintrc-all`**

This example config lists all of the rules and their primary options. You can remove (or turn off) the rules you don't want and edit the primary option of each rule to your liking.

**`.stylelintrc-recommended`**

This example config is trimmed down to only contain opinionated recommended defaults.

### Stylelint for Grunt
```bash
$ npm install -D grunt-stylelint stylelint
```
```javascript
// Example Grunt config
stylelint: {
	options: {
		configFile: '.stylelintrc',
		formatter: 'string',
		ignoreDisables: false,
		failOnError: true,
		reportNeedlessDisables: false,
		syntax: 'scss' // available syntaxes: 'scss', 'less', 'css'
	},
    all: ['path/to/**/*.scss']
}
```

Add .stylelintrc file containing the rules to your project. If you're using an editor plugin, it should preferably be in the project root.

To ignore files you can create a .stylelintignore file and add the files/filetypes you wish to ignore.

**`.stylelintignore`**
```json
fileToIgnore/*.scss
```

# ESLint


### Helpful links
* [ESLint explanation of all rule types](https://eslint.org/docs/rules/)
* [Create your own rules file for ESLint](https://eslint.org/demo/ )
* [ESLint integrations](https://eslint.org/docs/user-guide/integrations) (*editor plugins are highly recommended*)
* [ESLint config options](http://eslint.org/docs/developer-guide/nodejs-api#cliengine)

***


### ESLint for Grunt Build
##### Add Node package
```
$ npm install --save-dev grunt-eslint
```

##### Add eslint.json file containing rules to project in same folder as gruntfile.js
* To ignore files you can create a .eslintignore file and add the file/filetypes you wish to ignore
```
fileToIgnore/*.js
```



##### Recommended grunt config with ESLint rules file

```javascript
grunt.initConfig({
	eslint: {
			options: {
				configFile: 'yourOwnEsLintConfig.json',
				quiet: true,
				colors: true
			},
			validate: ['allYourJsFilesToLint/*.js']
		},
});
grunt.loadNpmTasks('grunt-eslint');
grunt.registerTask('lint', ['eslint']);
```
***

/**
 * cssSupports - (ES6 VERSION) Small function for testing browser CSS support
 *
 * @param  {string} features The feature(s) to be tested
 * @return {boolean}         Single value: returns true if feature is supported
 * @return {object}          Multiple: returns object with { property: true, property2: false }
 */

const supports = function(...features) {
    if(typeof features === 'undefined') return;

    features = features || '';

    let multi = {},
        single;

    features.length > 1 ? features.map(f => {
        f in document.body.style ? multi[f] = true : multi[f] = false;
    }) : single = features in document.body.style;

    return features.length > 1 ? multi : single;
}

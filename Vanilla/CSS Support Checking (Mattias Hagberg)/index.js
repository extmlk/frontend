/**
 * cssSupports - Small function for testing browser CSS support
 *
 * @param  {string} features The feature to be tested
 * @return {boolean}         Single value: returns true if feature is supported
 * @return {object}          Multiple: returns object with { property: true, property2: false }
 */

var supports = function(features) {
    if(typeof features === 'undefined') return;

    features = features || '';

    var args = (arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments));
    var multi = {};

    arguments.length > 1 && args.map(function(f) {
        f in document.body.style ? multi[f] = true : multi[f] = false;
    }, this);

    return arguments.length > 1 ? multi : features in document.body.style;
}

## Synopsis
A super-simple <1KB function for testing browser CSS support. Useful for stuff
like feature testing for newer CSS properties, such as `grid`, `flexbox`,
and `clip-path`.

### Usage

The function can be used in two ways:

1. By passing in a single property, and get a `boolean` in return
2. By passing in multiple properties, and get an iterable `object` in return

#### Single property

```javascript
supports('flex')                    // true
supports('display')                 // true
supports('clip-path')               // true
supports('foo')                     // false
supports('bar')                     // false

var checkFlex = supports('flex');

checkFlex;                          // true
```

#### Multiple properties

```javascript
supports('flex', 'grid', 'object-fit', 'foobar')
    // -> {flex: true, grid: true, object-fit: true, foobar: false}

let foobar = 'foobar';

supports('flex', 'grid', 'object-fit', foobar)
    // -> {flex: true, grid: true, object-fit: true, foobar: false}

const multiple = supports('flex', 'grid');

multiple;                           // {flex: true, grid: true}
multiple.grid;                      // true
multiple.foobar;                    // undefined
```

#### Bonus!
You can also extend the `Object.prototype` to check if *all* properties are
supported in the browser. Will return a `boolean` with `true` if all properties
are supported.

```javascript
Object.prototype.allSupported = function() {
    for (var i in this) {
        if (this[i] === true) return false;
    }
    return true;
}

const multiple = supports('flex', 'grid');

multiple.allSupported();            // true
```

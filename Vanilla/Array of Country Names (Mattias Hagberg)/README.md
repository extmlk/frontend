## Synopsis
A simple list of country names, divided into key-value pairs.


### Usage

This array can be used in several different ways. For example, you can use it in a plain ol' `for` loop, like this:
```javascript
for (var i in countries) {
	var country = countries[i];     // the entire array
	var countryKey = country.key;   // the country keys
	var countryVal = country.value; // the country values

	console.log(countryKey + ': ' + countryVal);
}
```

Or you can use it with the ES6 `map()`:

```javascript
const countryList = countries.map(country => console.dir({ key: country.key, value: country.value }));
```
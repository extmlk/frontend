## Synopsis
A minimal 1KB function for doing AJAX requests. By attaching the function to
the `window` object, we get rid of pain-in-the-ass issues like scoping.

### Usage

The function can be used for doing several types of requests. Passing only an
URL to the function will do a `GET` request (`window.Ajax(url)`). Passing in
several properties is possible, and the options are as follows:

```javascript
/**
 * @param  {string} url         URL to resource
 * @param  {string} method      (Optional) Defaults to 'GET'
 * @param  {object} data        (Optional) Data to send with e.g. 'POST' and 'PUT'
 * @param  {string} contentType (Optional) Content-Type setting
 * @return {promise}            Resolving and rejecting is handled on call
 */

window.Ajax(url, method, data, contentType)
```

#### Requirements
ES6, and support for [Promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises).

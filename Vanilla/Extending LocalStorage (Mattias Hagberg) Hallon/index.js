/**
 * Storage.prototype.setObject - Easily store objects in localStorage
 *
 * @param  {string} key   Name of object to be stored
 * @param  {object} value Object to be stored
 */

export const setObj = (key, value) => {
    Storage.prototype.setObject = (key, value) => {
        this.setItem(key, JSON.stringify(value));
    };
    return localStorage.setObject(key, value);
};

/**
 * Storage.prototype.getObject - Easily fetch objects from localStorage
 *
 * @param  {string} key   Reference to stored object
 * @return {object}
 */

export const getObj = key => {
    Storage.prototype.getObject = key => {
        let value = this.getItem(key);
        return value && JSON.parse(value);
    };
    return localStorage.getObject(key);
};
